<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PagesAccessTest extends WebTestCase
{
    public function testAccessHome()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAccessDisplayProfil()
    {
        $client = static::createClient();
        $client->request('GET', '/displayProfil/{id}');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAccessDisplayUser()
    {
        $client = static::createClient();
        $client->request('GET', '/displayUser/{id}');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAccessDisplayBrame()
    {
        $client = static::createClient();
        $client->request('GET', '/displayBrame/{id}');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAccessDisplayHashtag()
    {
        $client = static::createClient();
        $client->request('GET', '/displayHashtag/{hashtag}');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}