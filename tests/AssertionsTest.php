<?php


namespace App\Tests;


use App\Entity\Comment;
use PHPUnit\Framework\TestCase;
use App\Entity\Post;
use App\Entity\User;

class AssertionsTest extends TestCase
{
    public function testNewUser()
    {
        $user = new User();

        $user->setEmail('test@test.fr');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword('123456');
        $user->setPseudo('Simplon');
        $user->setFirstName('FirstName');
        $user->setLastName('LastName');

        $email = $user->getEmail();
        $role = $user->getRoles();
        $password = $user->getPassword();
        $pseudo = $user->getPseudo();
        $firstname = $user->getFirstName();
        $lastname = $user->getLastName();

        $this->assertEquals('test@test.fr', $email);
        $this->assertEquals(['ROLE_USER'], $role);
        $this->assertEquals('123456', $password);
        $this->assertEquals('Simplon', $pseudo);
        $this->assertEquals('FirstName', $firstname);
        $this->assertEquals('LastName', $lastname);
    }

    public function testNewBrame()
    {
        $brame = new Post();

        $brame->setMessage('Test des posts');
        $brame->setHashtag('Simplon');
        $date = new \DateTime('2000-00-00 00:00:00');
        $brame->setCreatedAt($date);

        $message = $brame->getMessage();
        $hashtag = $brame->getHashtag();
        $created = $brame->getCreatedAt();

        $this->assertEquals('Test des posts', $message);
        $this->assertEquals('Simplon', $hashtag);
        $this->assertEquals($date->getTimestamp(), $created->getTimestamp());
    }

    public function testNewComment()
    {
        $comment = new Comment();

        $comment->setMessage('Test des commentaires');
        $date = new \DateTime('2000-00-00 00:00:00');
        $comment->setCreatedAt($date);

        $content = $comment->getMessage();
        $created = $comment->getCreatedAt();

        $this->assertEquals('Test des commentaires', $content);
        $this->assertEquals($date->getTimestamp(), $created->getTimestamp());
    }
}
