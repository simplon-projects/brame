<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin de Brame');
    }

    public function configureMenuItems(): iterable
    {
       return [
           yield MenuItem::linkToCrud('User', 'fa fa-user', User::class),
           yield MenuItem::linkToCrud('Post', 'fa fa-file-text', Post::class),
           yield MenuItem::linkToCrud('Comment', 'fa fa-comment', Comment::class),
           yield MenuItem::linkToLogout('Se déconnecter', 'fa fa-sign-out', '/'),
           ];
    }
}
