<?php

namespace App\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(): Response
    {
        return $this->render('pages/index.html.twig');
    }

    /**
     * @Route("/displayProfil/{id}", name="displayProfil")
     * @param $id
     * @return Response
     */
    public function displayProfil($id): Response
    {
        return $this->render('pages/displayProfil.html.twig', [
            'profilId' => $id
        ]);
    }

    /**
     * @Route("/displayUser/{id}", name="displayUser")
     * @return Response
     */
    public function displayUserShow($id): Response
    {
        return $this->render('pages/displayUser.html.twig', [
            'userId' => $id
        ]);
    }

    /**
     * @Route("/displayBrame/{id}", name="displayBrame")
     * @param $id
     * @return Response
     */
    public function displayBrame($id): Response
    {
        return $this->render('pages/displayBrame.html.twig', [
            'postId' => $id
        ]);
    }

    /**
     * @Route("/displayHashtag/{hashtag}", name="displayHashtag")
     * @param $hashtag
     * @return Response
     */
    public function displayHashtag($hashtag): Response
    {
        return $this->render('pages/displayHashtag.html.twig', [
            'hashtag' => $hashtag
        ]);
    }
}
