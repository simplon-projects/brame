<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class BrameController extends AbstractController
{

    /**
     * @Route("/newBrame", name="newBrame")
     */
    public function newBrame(): Response
    {
        return $this->render('pages/newBrame.html.twig');
    }

    /**
     * @Route("/modifyBrame/{id}", name="modifyBrame")
     */
    public function modifyBrame($id)
    {
        return $this->render('pages/modifyBrame.html.twig', [
            'postId' => $id
        ]);
    }

    /**
     * @Route("/modifyUser/{id}", name="modifyUser")
     * @param $id
     * @return Response
     */
    public function modifyUser($id)
    {
        return $this->render('pages/modifyUser.html.twig', [
            'userId' => $id
        ]);
    }
}
