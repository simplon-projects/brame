<?php

namespace App\Controller;

use http\Env\Request;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController extends AbstractController
{
    /**
     * @Route("/redirect", name="redirect")
     */
    public function index(): Response
    {
        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles()))

        {
            return $this->redirectToRoute('admin');
        }

        $user = $this -> getUser();
        $userId = $user ->getId();

        return $this->redirectToRoute('displayUser', ['id'=> $userId]);
    }
}
