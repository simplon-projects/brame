<?php


namespace App\Controller\Api;


use Symfony\Component\Security\Core\Security;

class CommentCreateController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function __invoke($data)
    {
        $data->setAuthorId($this->security->getUser());

        return $data;
    }
}