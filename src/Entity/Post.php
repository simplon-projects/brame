<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get",
 *      "post"={
 *          "controller"=App\Controller\Api\PostCreateController::class
 *      }
 *     },
 *     attributes={
 *      "order"={"createdAt":"DESC"}
 *     },
 *     itemOperations={
 *      "get",
 *      "put",
 *      "delete"
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties={"hashtag": "exact"})
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=140)
     * @Assert\Length(
     *      min = 2,
     *      max = 140,
     *      minMessage = "Ce champ doit contenir au moins {{ limit }} caractères",
     *      maxMessage = "Ce champ ne doit pas dépasser {{ limit }} caractères"
     *     )
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Length(
     *      min = 2,
     *      max = 20,
     *      minMessage = "Ce champ doit contenir au moins {{ 2 }} caractères",
     *      maxMessage = "Ce champ ne doit pas dépasser {{ 20 }} caractères"
     *     )
     */
    private $hashtag;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
     */
    private $authorId;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="likedPosts")
     */
    private $usersLikes;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->usersLikes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getHashtag(): ?string
    {
        return $this->hashtag;
    }

    public function setHashtag(string $hashtag): self
    {
        $this->hashtag = $hashtag;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAuthorId(): ?User
    {
        return $this->authorId;
    }

    public function setAuthorId(?User $authorId): self
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsersLikes(): Collection
    {
        return $this->usersLikes;
    }

    public function addUsersLike(User $usersLike): self
    {
        if (!$this->usersLikes->contains($usersLike)) {
            $this->usersLikes[] = $usersLike;
        }

        return $this;
    }

    public function removeUsersLike(User $usersLike): self
    {
        $this->usersLikes->removeElement($usersLike);

        return $this;
    }
}
