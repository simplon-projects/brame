// Variable de la div html '#postDetails'
const postDetails = document.getElementById('postDetails');

// Valeur ID du post selectionne
const postId = postDetails.dataset.id;

// CREATION DE TOUS LES ÉLÉMENTS HTML

// container flex qui contient 2 containers pour entête du post
const brameHeader = document.createElement('div');
brameHeader.className = 'd-flex flex-wrap flex-row justify-content-between align-items-start';
const headerLeft = document.createElement('div');
const headerRight = document.createElement('div');
headerRight.className = "py-1"
brameHeader.append(headerLeft, headerRight);

const brameAuteur = document.createElement('div');
brameAuteur.className = 'd-flex flex-wrap flex-row align-items-start pb-1';
headerLeft.appendChild(brameAuteur);
const auteurName = document.createElement('p');
auteurName.className = 'auteurDisplayBrame text-primary font-weight-bold py-0 pr-1 my-0';
brameAuteur.appendChild(auteurName);
const auteurPseudo = document.createElement('a');
auteurPseudo.className = 'pseudoDisplayBrame py-1 my-0';
brameAuteur.appendChild(auteurPseudo);
        
const createdAt = document.createElement('div');
headerLeft.appendChild(createdAt);
const datetime = document.createElement('p');
datetime.className = 'dateDisplayBrame text-primary pb-2 my-0';
createdAt.appendChild(datetime);

const brameHashtag = document.createElement('a');
brameHashtag.className = 'hashBrameDisplay';
headerRight.appendChild(brameHashtag);

const numberCom = document.createElement('p');
numberCom.className = 'numComment text-primary m-0';
headerLeft.appendChild(numberCom);

const brameMessageDiv = document.createElement('div');
brameMessageDiv.className = 'messageDisplayBrame bg-secondary p-3 my-3 shadow-sm rounded text-primary';
const brameMessage = document.createElement('p');
brameMessageDiv.append(brameMessage);

postDetails.append(brameHeader, brameMessageDiv);

//les boutons
const likeButton = document.createElement('a');
const commentButton = document.createElement('a');
const modifyBrameButton = document.createElement('a');
const deleteBrameButton = document.createElement('a');

// FETCH POUR AFFICHER LE POST SELECTIONNE
fetch(`/api/posts/${postId}`)
    .then((response) => response.json(),
    )
    .then((result) => {

        // A supprimer avant rendu final
        console.log('fetch du post');
        console.log(result);
        // FETCH POUR AFFICHER LES INFOS DU PROFIL
        fetch(`${result.authorId}`)
            .then((response) => response.json(),
            )
            .then((result) => {

                // nom prenom et pseudo de l'auteur
                auteurName.innerHTML = `${result.firstName} ${result.lastName}`;
                auteurPseudo.href = '/displayProfil/'+result.id;
                auteurPseudo.innerHTML = '@'+result.pseudo;

                // Si ID user connecté = ID user author du post display les bouttons
                if(userId && userId === result.id)
                {
                    buttonDiv.appendChild(modifyBrameButton);
                    buttonDiv.appendChild(deleteBrameButton);
                }
            })
        
        // hashtag
        brameHashtag.href = '/displayHashtag/'+result.hashtag;
        brameHashtag.innerHTML = '# '+result.hashtag;
        
        // date et heure de création du post
        const date = new Date(result.createdAt);
        const dateOfCreation = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();
        const timeOfCreation = date.getHours()+"h"+date.getMinutes();
        datetime.innerText = dateOfCreation + " - " + timeOfCreation;

        // contenu du post (message)
        brameMessage.innerText = result.message;

        // div pour les boutons
        const buttonDiv = document.createElement('div');
        buttonDiv.className = 'd-flex flex-row flex-wrap justify-content-end my-3';
        postDetails.appendChild(buttonDiv);
    
        // bouton pour liker - work in progress

        likeButton.href = '#';
        likeButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

        const iconLikePath =  require('../images/like.png');
        const iconLikeImg = `<img src="${iconLikePath.default}" alt="like">`;
        const iconLikePath2 =  require('../images/like2.png');
        const iconLikeImg2 = `<img src="${iconLikePath2.default}" alt="like">`;

        likeButton.innerHTML = iconLikeImg2;
        
        likeButton.addEventListener("mouseover", function( event ) {
            event.target.innerHTML = iconLikeImg;
        });
        likeButton.addEventListener("mouseleave", function( event ) {   
            event.target.innerHTML = iconLikeImg2;
        });
    
        // Bouton pour commenter - redirige sur displayBrame

        commentButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

        const iconComPath =  require('../images/comment.png');
        const iconComImg = `<img src="${iconComPath.default}" alt="comment">`;
        const iconComPath2 =  require('../images/comment2.png');
        const iconComImg2 = `<img src="${iconComPath2.default}" alt="comment">`;

        commentButton.innerHTML = iconComImg2;
        
        commentButton.addEventListener("mouseover", function( event ) {
            event.target.innerHTML = iconComImg;
        });
        commentButton.addEventListener("mouseleave", function( event ) {   
            event.target.innerHTML = iconComImg2;
        });
    
        // Bouton pour modifier - redirige sur modifyBrame
        // Append uniquement si id user = id autheur du post

        modifyBrameButton.href = '/modifyBrame/'+result.id;
        modifyBrameButton.className = "edit bouton mx-2 shadow d-flex justify-content-center align-items-center";

        const iconEdPath =  require('../images/edit.png');
        const iconEdImg = `<img src="${iconEdPath.default}" alt="edit">`;
        const iconEdPath2 =  require('../images/edit2.png');
        const iconEdImg2 = `<img src="${iconEdPath2.default}" alt="edit">`;

        modifyBrameButton.innerHTML = iconEdImg2;
        
        modifyBrameButton.addEventListener("mouseover", function( event ) {
            event.target.innerHTML = iconEdImg;
        });
        modifyBrameButton.addEventListener("mouseleave", function( event ) {   
            event.target.innerHTML = iconEdImg2;
        });
    
        // Bouton pour supprimer - fetch l'api pour supprimer le post
        // Append uniquement si id user = id autheur du post
        deleteBrameButton.href = '#';
        deleteBrameButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

        const iconSupPath =  require('../images/trash.png');
        const iconSupImg = `<img src="${iconSupPath.default}" alt="delete">`;
        const iconSupPath2 =  require('../images/trash2.png');
        const iconSupImg2 = `<img src="${iconSupPath2.default}" alt="delete">`;

        deleteBrameButton.innerHTML = iconSupImg2;
        
        deleteBrameButton.addEventListener("mouseover", function( event ) {
            event.target.innerHTML = iconSupImg;
        });
        deleteBrameButton.addEventListener("mouseleave", function( event ) {   
            event.target.innerHTML = iconSupImg2;
        });
    
        buttonDiv.append(commentButton, likeButton);
        
        // Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
        // Icons made by <a href="https://www.flaticon.com/authors/gregor-cresnar" title="Gregor Cresnar">Gregor Cresnar</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
        // Icons made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>        

        // Lorsque click sur le bouton supprimer
        deleteBrameButton.addEventListener('click', function() {

            // FETCH POUR SUPPRIMER LE POST EN BASE DE DONNEES
            fetch(`/api/posts/${result.id}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'delete'
            })
                .then(function(res){
                    console.log('test');
                    window.location.href = '/';
                })
                .catch(function(res){console.log(res)})
        })
    });

// ACTION LORSQUE CLICK SUR BOUTON COMMENTAIRE
const commentBrameForm = document.getElementById('commentBrameForm'); // form

document.addEventListener('DOMContentLoaded', (event) => {
    commentButton.addEventListener('click', function() {
        if (userId === 'Not connected') {
            swal({ 
                title: "Connexion requise",
                text: "vous devez vous connecter pour publier un commentaire!",
                icon: "error",
            }). then(function(result){
                window.location.href = "/login";
            });
        } else {
            if(commentBrameForm.classList.contains('clicked')) {
                commentBrameForm.classList.remove('clicked');
            }
            else {
                commentBrameForm.classList.add('clicked');
            }
        }
    });
});

// swal({
//     title: $(e.target).attr('data-shortname'),
//     text: 'a été ajouté aux favoris',
//     icon: './assets/images/bigstar.svg',
//     className: 'sweetalert',
//     buttons: false,
//     timer: 2000,
//   });

// FETCH POUR AFFICHER LES COMMENTAIRES DU POST SELECTIONNE
fetch(`/api/comments?postId=${postId}`)
    .then((response) => response.json(), // Si la requete est valide
    )
    .then((result) => {

        // A supprimer avant rendu final
        console.log('fetch des commentaires');
        console.log(result);

        // Récupère les informations
        const data = result['hydra:member'];

        // Pour le nombre de commmentaires
        const numComment = result['hydra:totalItems'];
        if (numComment < 2) {
            numberCom.innerText = `${numComment} commentaire`;
        } else {
            numberCom.innerText = `${numComment} commentaires`;
        }
    

        // Variable de la div html '#postComment'
        const postComment = document.getElementById('postComment');
        const title = document.createElement('p');
        title.innerText = "Commentaires";
        title.className = "h4 text-white ml-auto"
        postComment.appendChild(title);

        for(i = 0; i < data.length; i++) {

            const result = data[i];

            // CREATION DE TOUS LES ÉLÉMENTS HTML
            const lastComment = document.createElement('div');
            lastComment.className = 'lastComment my-4';
            postComment.appendChild(lastComment);

            const commentAuteur = document.createElement('div');
            commentAuteur.className = 'd-flex flex-wrap align-items-center flex-row';
            const auteurName = document.createElement('p');
            auteurName.className = 'text-primary font-weight-bold py-2 my-0 pr-2';
            commentAuteur.appendChild(auteurName);
            const auteurPseudo = document.createElement('a');
            auteurPseudo.className = 'pseudoComment py-2';
            commentAuteur.appendChild(auteurPseudo);

            const brameCommentDiv = document.createElement('div');
            brameCommentDiv.className = 'commentPost bg-secondary p-2 shadow-sm rounded text-primary d-flex flex-sm-row flex-column';
            const brameComment = document.createElement('p');
            brameComment.className = 'flex-grow-1';
            brameCommentDiv.appendChild(brameComment);
            const datetime = document.createElement('p');
            datetime.className = 'dateComment text-primary text-right flex-shrink-0 align-self-end px-2 m-0';
            const date = new Date(result.createdAt);
            brameCommentDiv.appendChild(datetime);

            lastComment.append(commentAuteur, brameCommentDiv);

            // contenu du post (commentaire) dans un container
            brameComment.innerHTML = result.message;

            // date et heure de création du commentaire
            const dateOfCreation = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();
            const timeOfCreation = date.getHours()+"h"+date.getMinutes();
            datetime.innerText = dateOfCreation + " - " + timeOfCreation;

            // FETCH POUR AFFICHER LES INFOS DU PROFIL
            fetch(`${result.authorId}`)
                .then((response) => response.json(), // Si la requete est valide
                )
                .then((result) => {

                    // nom prenom et pseudo de l'auteur dans container flex
                    auteurName.innerHTML = `${result.firstName} ${result.lastName}`;
                    auteurPseudo.href = '/displayProfil/'+result.auteurId;
                    auteurPseudo.innerHTML = '@'+result.pseudo;
                })
        }
    });


// LORSQUE LE FORMULAIRE EST SUBMIT
commentBrameForm.addEventListener('submit', function() {

    const commentBrame = document.getElementById('commentBrame').value;

    // FETCH POUR ECRIRE LE COMMENTAIRE EN BASE DE DONNEES
    fetch('/api/comments', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'POST',

        // Ici les informations que l'on rentre en bdd = nomDeLaTable: variableAvecLinfo
        body:JSON.stringify({postId: postId, message: commentBrame})
    })
        .then(function(res){
            window.location.href = `/displayBrame/${postId}`;
        })
        .catch(function(res){console.log(res)})
})