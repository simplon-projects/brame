// Variable de la div html '#displayUser'
const displayUser = document.getElementById('displayUser');

// Valeur du profil selectionne
const userId = displayUser.dataset.id;

// FETCH POUR AFFICHER LES INFOS DU PROFIL SELECTIONNE
fetch(`/api/users/${userId}`)
    .then((response) => response.json(), // Si la requete est valide
    )
    .then((result) => {

        // A supprimer avant rendu final
        console.log('fetch du profil');
        console.log(result);

        // User img
        // const userImg = document.createElement('img');
        // userImg.setAttribute = '("src", "#")'; // à changer pour l'adresse en bdd (cf controller ? ) de là où est l'img
        // userImg.setAttribute = '("alt", "")';
        // userImg.className = ('text-center');


        // User Identity (h1)
        const displayUserIdentity = document.createElement('h1');
        displayUserIdentity.innerText = result.firstName + ' ' + result.lastName;
        displayUserIdentity.className = 'text-primary text-center my-5 font-weight-bold';

        // SECTION USER INFOS
        const displayUserInfo = document.createElement('div');
        displayUserInfo.className = 'my-5';

        // INSIDE USER INFOS
        const displayUserTitleInfos = document.createElement('h2');
        displayUserTitleInfos.innerText = 'Profil';
        displayUserTitleInfos.className = 'h4 text-white mb-3';

        // First name
        const displayUserFName = document.createElement('p');
        displayUserFName.className = 'mb-1 ml-5';
        displayUserFName.innerHTML = '<span class="font-weight-bold">Prénom : </span>' + result.firstName;

        // Last name
        const displayUserLName = document.createElement('p');
        displayUserLName.className = 'mb-1 ml-5';
        displayUserLName.innerHTML = '<span class="font-weight-bold">Nom : </span>' + result.lastName;

        // Pseudo
        const displayUserPseudo = document.createElement('p');
        displayUserPseudo.className = 'mb-1 ml-5';
        displayUserPseudo.innerHTML = '<span class="font-weight-bold">Pseudo : </span>' + result.pseudo;

        //Email
        const displayUserEmail = document.createElement('p');
        displayUserEmail.className = 'mb-1 ml-5';
        displayUserEmail.innerHTML = '<span class="font-weight-bold">Email : </span>' + result.email;

        //Boutton
        const displayUserButtonModify = document.createElement('button');
        displayUserButtonModify.className = 'btn btn-primary mt-3';
        displayUserButtonModify.innerText = 'Modifier';

        //Redirection à la page modifyUser pour modifier l'info du profil
        displayUserButtonModify.addEventListener('click', function() {

            window.location.href = '/modifyUser/'+result.id;
        })

        displayUserInfo.append(displayUserTitleInfos, displayUserFName, displayUserLName,
            displayUserPseudo, displayUserEmail, displayUserButtonModify);

        // SECTION USER BRAMES
        const displayUserBrames = document.createElement('div');
        displayUserBrames.className = 'my-5';

        // INSIDE USER BRAMES
        const displayUserTitleBrames = document.createElement('h2');
        displayUserTitleBrames.className = 'h4 text-white mb-3';
        displayUserTitleBrames.innerText = 'Brames';

        displayUserBrames.append(displayUserTitleBrames);

        // FETCH POUR LES POSTS DU PROFIL SELECTIONNE

        const displayUserComment = result.posts;

        // A supprimer avant rendu final
        console.log('fetch des brames du profil');
        console.log(displayUserComment);

        for(i = 0; i < displayUserComment.length; i++) {
            const result = displayUserComment[i];

            const displayUserBrame = document.createElement('div');
            displayUserBrame.className = 'messageDisplayBrame py-3 my-2 ml-5 border-top border-secondary';

            fetch(`${result}`)
                .then((response) => response.json(), // Si la requete est valide
                )
                .then((result) => {
                    const displayUserBrameHeader = document.createElement('div');
                    displayUserBrameHeader.className = "d-flex flex-row flex-wrap-reverse justify-content-between";

                    const displayUserBrameDatetime = document.createElement('p');
                    displayUserBrameDatetime.className = 'dateBrameProfil text-primary pb-2 my-0';
                    const date = new Date(result.createdAt);
                    const displayUserDateOfCreation = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();
                    const displayUserTimeOfCreation = date.getHours()+"h"+date.getMinutes();
                    displayUserBrameDatetime.innerText = displayUserDateOfCreation + " - " + displayUserTimeOfCreation;

                    const displayUserBrameHashtag = document.createElement('a');
                    displayUserBrameHashtag.className = 'hashBrameProfil';
                    displayUserBrameHashtag.href = '/displayHashtag/'+result.hashtag;
                    displayUserBrameHashtag.innerHTML = '# '+result.hashtag;

                    const displayUserBrameMessageLink = document.createElement('a');
                    displayUserBrameMessageLink.href = '/displayBrame/'+result.id;

                    const displayUserBrameMessageDiv = document.createElement('div');
                    displayUserBrameMessageDiv.className = 'messagePostIndex bg-secondary p-2 my-1 shadow-sm rounded text-primary';
                    displayUserBrameMessageLink.appendChild(displayUserBrameMessageDiv);

                    const displayUserBrameMessage = document.createElement('p');
                    displayUserBrameMessage.innerText = result.message;
                    displayUserBrameMessageDiv.appendChild(displayUserBrameMessage);

                    //nombre de commentaire
                    const numberCom = document.createElement('a');
                    const numComContent = document.createElement('p');
                    numComContent.className = 'numComment';
                    numberCom.appendChild(numComContent);

                    // div pour les boutons
                    const buttonDiv = document.createElement('div');
                    buttonDiv.className = 'd-flex flex-row flex-wrap justify-content-end my-3';
                    //displayUserBrameMessage.appendChild(buttonDiv);

                    // bouton pour liker - work in progress
                    const likeButton = document.createElement('a');
                    likeButton.href = '#';
                    likeButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

                    const iconLikePath =  require('../images/like.png');
                    const iconLikeImg = `<img src="${iconLikePath.default}" alt="like">`;
                    const iconLikePath2 =  require('../images/like2.png');
                    const iconLikeImg2 = `<img src="${iconLikePath2.default}" alt="like">`;

                    likeButton.innerHTML = iconLikeImg2;

                    likeButton.addEventListener("mouseover", function( event ) {
                        event.target.innerHTML = iconLikeImg;
                    });
                    likeButton.addEventListener("mouseleave", function( event ) {
                        event.target.innerHTML = iconLikeImg2;
                    });

                    // Bouton pour commenter - redirige sur displayBrame
                    const commentButton = document.createElement('a');
                    commentButton.href = '/displayBrame/'+result.id;
                    commentButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

                    const iconComPath =  require('../images/comment.png');
                    const iconComImg = `<img src="${iconComPath.default}" alt="comment">`;
                    const iconComPath2 =  require('../images/comment2.png');
                    const iconComImg2 = `<img src="${iconComPath2.default}" alt="comment">`;

                    commentButton.innerHTML = iconComImg2;

                    commentButton.addEventListener("mouseover", function( event ) {
                        event.target.innerHTML = iconComImg;
                    });
                    commentButton.addEventListener("mouseleave", function( event ) {
                        event.target.innerHTML = iconComImg2;
                    });

                    // Bouton pour modifier - redirige sur modifyBrame
                    // Append uniquement si id user = id autheur du post
                    const modifyBrameButton = document.createElement('a');
                    modifyBrameButton.href = '/modifyBrame/'+result.id;
                    modifyBrameButton.className = "edit bouton mx-2 shadow d-flex justify-content-center align-items-center";

                    const iconEdPath =  require('../images/edit.png');
                    const iconEdImg = `<img src="${iconEdPath.default}" alt="edit">`;
                    const iconEdPath2 =  require('../images/edit2.png');
                    const iconEdImg2 = `<img src="${iconEdPath2.default}" alt="edit">`;

                    modifyBrameButton.innerHTML = iconEdImg2;

                    modifyBrameButton.addEventListener("mouseover", function( event ) {
                        event.target.innerHTML = iconEdImg;
                    });
                    modifyBrameButton.addEventListener("mouseleave", function( event ) {
                        event.target.innerHTML = iconEdImg2;
                    });

                    // Bouton pour supprimer - fetch l'api pour supprimer le post
                    // Append uniquement si id user = id autheur du post
                    const deleteBrameButton = document.createElement('a');
                    deleteBrameButton.href = '#';
                    deleteBrameButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

                    const iconSupPath =  require('../images/trash.png');
                    const iconSupImg = `<img src="${iconSupPath.default}" alt="delete">`;
                    const iconSupPath2 =  require('../images/trash2.png');
                    const iconSupImg2 = `<img src="${iconSupPath2.default}" alt="delete">`;

                    deleteBrameButton.innerHTML = iconSupImg2;

                    deleteBrameButton.addEventListener("mouseover", function( event ) {
                        event.target.innerHTML = iconSupImg;
                    });
                    deleteBrameButton.addEventListener("mouseleave", function( event ) {
                        event.target.innerHTML = iconSupImg2;
                    });

                    buttonDiv.append(commentButton, likeButton, modifyBrameButton, deleteBrameButton);

                    // Lorsque click sur le bouton supprimer
                    deleteBrameButton.addEventListener('click', function() {

                        // FETCH POUR SUPPRIMER LE POST EN BASE DE DONNEES
                        fetch(`/api/posts/${result.id}`, {
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            method: 'delete'
                        })
                            .then(function(res){
                                console.log('test');
                                window.location.href = '/';
                            })
                            .catch(function(res){console.log(res)})
                    })

                    // FETCH POUR RÉCUPÉRER LE NOMBRE DE COMMENTAIRES D'UN POST
                    numberCom.href = '/displayBrame/'+result.id;
                    const postId = result.id;
                    fetch(`/api/comments?postId=${postId}`)
                .then((response) => response.json(), // Si la requete est valide
                    )
                        .then((result) => {
                            // A supprimer avant rendu final
                            console.log('fetch des commentaires :', result);
                            // Récupère les informations
                            const numComment = result['hydra:totalItems'];
                            if (numComment < 2) {
                                numComContent.innerText = `${numComment} commentaire`;
                            } else {
                                numComContent.innerText = `${numComment} commentaires`;
                            }
                        });

                    displayUserBrameHeader.append(displayUserBrameDatetime, displayUserBrameHashtag);
                    displayUserBrame.append(displayUserBrameHeader, displayUserBrameMessageLink, numComContent, buttonDiv);
                });

            displayUserBrames.append(displayUserBrame);
        }

        displayUser.append(displayUserIdentity, displayUserInfo, displayUserBrames); // userImg , userMentions

    });