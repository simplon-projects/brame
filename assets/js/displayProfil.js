// Variable de la div html '#displayProfil'
const displayProfil = document.getElementById('displayProfil');

// Valeur du profil selectionne
const profilId = displayProfil.dataset.id;

// FETCH POUR AFFICHER LES INFOS DU PROFIL SELECTIONNE
fetch(`/api/users/${profilId}`)
    .then((response) => response.json(), // Si la requete est valide
    )
    .then((result) => {

        // A supprimer avant rendu final
        console.log('fetch du profil');
        console.log(result);

        // User img
        // const userImg = document.createElement('img');
        // userImg.setAttribute = '("src", "#")'; // à changer pour l'adresse en bdd (cf controller ? ) de là où est l'img
        // userImg.setAttribute = '("alt", "")';
        // userImg.className = ('text-center');

        // User Identity (h1)
        const userIdentity = document.createElement('h1');
        userIdentity.innerText = result.firstName + ' ' + result.lastName;
        userIdentity.className = 'text-primary text-center my-5 font-weight-bold';

        // SECTION USER INFOS
        const userInfo = document.createElement('div');
        userInfo.className = 'my-5';

        // INSIDE USER INFOS
        const titleInfos = document.createElement('h2');
        titleInfos.innerText = 'Profil';
        titleInfos.className = 'h4 text-white mb-3';

        // First name
        const fName = document.createElement('p');
        fName.className = 'mb-1 ml-5';
        fName.innerHTML = '<span class="font-weight-bold">Prénom : </span>' + result.firstName;

        // Last name
        const lName = document.createElement('p');
        lName.className = 'mb-1 ml-5';
        lName.innerHTML = '<span class="font-weight-bold">Nom : </span>' + result.lastName;

        // Pseudo
        const pseudo = document.createElement('p');
        pseudo.className = 'mb-1 ml-5';
        pseudo.innerHTML = '<span class="font-weight-bold">Pseudo : </span>' + result.pseudo;

        userInfo.append(titleInfos, fName, lName, pseudo);

        // SECTION USER BRAMES
        const userBrames = document.createElement('div');
        userBrames.className = 'my-5';

        // INSIDE USER BRAMES
        const titleBrames = document.createElement('h2');
        titleBrames.className = 'h4 text-white mb-3';
        titleBrames.innerText = 'Brames';

        userBrames.append(titleBrames);

        // FETCH POUR LES POSTS DU PROFIL SELECTIONNE

        const userComment = result.posts;

        // A supprimer avant rendu final
        console.log('fetch des brames du profil');
        console.log(userComment);

        for(i = 0; i < userComment.length; i++) {
            const result = userComment[i];

            const brame = document.createElement('div');
            brame.className = 'py-3 my-2 ml-5 border-top border-secondary';

            fetch(`${result}`)
                .then((response) => response.json(), // Si la requete est valide
                )
                .then((result) => {
                    const brameHeader = document.createElement('div');
                    brameHeader.className = "d-flex flex-row flex-wrap-reverse justify-content-between";
                    
                    const brameDatetime = document.createElement('p');
                    brameDatetime.className = 'dateBrameProfil text-primary pb-2 my-0';
                    const date = new Date(result.createdAt);
                    const dateOfCreation = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();
                    const timeOfCreation = date.getHours()+"h"+date.getMinutes();
                    brameDatetime.innerText = dateOfCreation + " - " + timeOfCreation;

                    const brameHashtag = document.createElement('a');
                    brameHashtag.className = 'hashBrameProfil';
                    brameHashtag.href = '/displayHashtag/'+result.hashtag;
                    brameHashtag.innerHTML = '# '+result.hashtag;

                    const brameMessageLink = document.createElement('a');
                    brameMessageLink.href = '/displayBrame/'+result.id;

                    const brameMessageDiv = document.createElement('div');
                    brameMessageDiv.className = 'messagePostIndex bg-secondary p-2 my-1 shadow-sm rounded text-primary';
                    brameMessageLink.appendChild(brameMessageDiv);

                    const brameMessage = document.createElement('p');
                    brameMessage.innerText = result.message;
                    brameMessageDiv.appendChild(brameMessage);

                    brameHeader.append(brameDatetime, brameHashtag);
                    brame.append(brameHeader, brameMessageLink);                
                });

            userBrames.append(brame);
        }

        displayProfil.append(userIdentity, userInfo, userBrames); // userImg , userMentions
    });