// Variable de la div html '#modifyBrameForm'
const modifyBrameForm = document.getElementById('modifyBrameForm');

// Valeur ID du post selectionne
const postId = modifyBrameForm.dataset.id;

// FETCH POUR AFFICHER LES VALEURS DU POST A MODIFIER
fetch(`/api/posts/${postId}`)
    .then((response) => response.json(),
    )
    .then((result) => {

        const messageBrame = document.getElementById('messageBrame');
        messageBrame.innerHTML = result.message;

        const brameHashtag = document.getElementById('brameHashtag');
        brameHashtag.value = result.hashtag;

        // LORSQUE LE FORMULAIRE EST SUBMIT
        modifyBrameForm.addEventListener('submit', function() {

            const inputMessage = document.getElementById('messageBrame').value;
            const hashtag = document.getElementById('brameHashtag').value;

            // FETCH POUR RE-ECRIRE LE POST EN BASE DE DONNEES
            fetch(`/api/posts/${postId}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'PUT',

                // Ici les informations que l'on rentre en bdd = nomDeLaTable: variableAvecLinfo
                body:JSON.stringify({message: inputMessage, hashtag: hashtag})
            })
                .then(function(res){
                    console.log('test');
                    window.location.href = '/displayUser/'+result.id;
                })
                .catch(function(res){console.log(res)})
        })

    })
