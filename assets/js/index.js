import {
    likeSystem
} from './likeSystem.js';

// Variable de la div html '#lastB²rameDisplay'
const brameDisplay = document.getElementById('lastBrameDisplay');

// FETCH POUR AFFICHER LES DERNIER POST SELECTIONNES
fetch('/api/posts')
    .then((response) => response.json(), )
    .then((query) => {

        // Récupère les informations
        const data = query['hydra:member'];

        // A supprimer avant rendu final
        console.log('test fetch');
        console.log(data);


        for (let i = 0; i < data.length; i++) {

            const result = data[i];

            // CREATION DE TOUS LES ÉLÉMENTS HTML
            const brame = document.createElement('div');
            brame.className = 'py-3 my-2 border-top border-secondary d-flex flex-column';
            brameDisplay.appendChild(brame);

            const brameHeader = document.createElement('div');
            brameHeader.className = 'd-flex flex-row flex-wrap justify-content-between';
            const headerLeft = document.createElement('div');
            const headerRight = document.createElement('div');
            brameHeader.append(headerLeft, headerRight);

            const brameHashtag = document.createElement('a');
            brameHashtag.className = 'hashPostIndex';
            headerRight.appendChild(brameHashtag);

            const brameAuteur = document.createElement('div');
            brameAuteur.className = 'd-flex flex-row flex-wrap pb-2';
            headerLeft.appendChild(brameAuteur);
            const auteurName = document.createElement('p');
            auteurName.className = 'text-primary font-weight-bold py-0 my-0 pr-2';
            brameAuteur.appendChild(auteurName);
            const auteurPseudo = document.createElement('a');
            auteurPseudo.className = 'pseudoPostIndex py-0 my-0 px-0';
            brameAuteur.appendChild(auteurPseudo);

            const createdAt = document.createElement('div');
            headerLeft.appendChild(createdAt);
            const datetime = document.createElement('p');
            datetime.className = 'datePostIndex text-primary pb-2 my-0';
            createdAt.appendChild(datetime);

            const brameMessageLink = document.createElement('a');
            const brameMessageDiv = document.createElement('div');
            brameMessageDiv.className = 'messagePostIndex bg-secondary p-2 my-1 shadow-sm rounded text-primary';
            brameMessageLink.appendChild(brameMessageDiv);
            const brameMessage = document.createElement('p');
            brameMessageDiv.appendChild(brameMessage);

            brame.append(brameHeader, brameMessageLink);

            // FETCH POUR AFFICHER LES INFOS DU PROFIL
            fetch(`${result.authorId}`)
                .then((response) => response.json(), )
                .then((result) => {

                    // nom prenom et pseudo de l'auteur dans container flex
                    auteurName.innerHTML = `${result.firstName} ${result.lastName}`;
                    auteurPseudo.href = '/displayProfil/' + result.id;
                    auteurPseudo.innerHTML = '@' + result.pseudo;

                    // Si ID user connecté = ID user author du post display les bouttons
                    if (userId && userId === result.id) {
                        buttonDiv.appendChild(modifyBrameButton);
                        buttonDiv.appendChild(deleteBrameButton);
                    }
                })

            // hashtag
            brameHashtag.href = '/displayHashtag/' + result.hashtag;
            brameHashtag.innerHTML = '# ' + result.hashtag;

            // contenu du post (message) dans un container
            brameMessageLink.href = '/displayBrame/' + result.id;
            brameMessage.innerHTML = result.message;

            // date et heure de création du post
            const date = new Date(result.createdAt);
            const dateOfCreation = ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear();
            const timeOfCreation = date.getHours() + "h" + date.getMinutes();
            datetime.innerText = dateOfCreation + " - " + timeOfCreation;

            // Div pour les boutons
            const buttonDiv = document.createElement('div');
            buttonDiv.className = 'd-flex flex-row flex-wrap justify-content-end my-3';
            brame.appendChild(buttonDiv);

            // bouton pour liker - work in progress
            const likeButton = document.createElement('a');
            likeButton.href = '#';
            likeButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconLikePath = require('../images/like.png');
            const iconLikeImg = `<img src="${iconLikePath.default}" alt="like">`;
            const iconLikePath2 = require('../images/like2.png');
            const iconLikeImg2 = `<img src="${iconLikePath2.default}" alt="like">`;

            likeButton.innerHTML = iconLikeImg2;

            likeButton.addEventListener("mouseover", function (event) {
                event.target.innerHTML = iconLikeImg;
            });
            likeButton.addEventListener("mouseleave", function (event) {
                event.target.innerHTML = iconLikeImg2;
            });

            // tableau des likes dans Post entity
            let usersLikes = result.usersLikes;

            let likesCounter = document.createElement('p');
            likesCounter.classList = 'likesCounter mr-2 text-primary';
            let numberOfLikes = result.usersLikes.length;
            likesCounter.innerText = numberOfLikes;

            // In likeSystem.js
            likeSystem(likeButton, userId, usersLikes, result, likesCounter);

            // Bouton pour commenter - redirige sur displayBrame
            // Bouton pour commenter - redirige sur displayBrame
            const commentButton = document.createElement('a');
            commentButton.href = '/displayBrame/'+result.id;
            commentButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconComPath = require('../images/comment.png');
            const iconComImg = `<img src="${iconComPath.default}" alt="comment">`;
            const iconComPath2 = require('../images/comment2.png');
            const iconComImg2 = `<img src="${iconComPath2.default}" alt="comment">`;

            commentButton.innerHTML = iconComImg2;

            commentButton.addEventListener("mouseover", function (event) {
                event.target.innerHTML = iconComImg;
            });
            commentButton.addEventListener("mouseleave", function (event) {
                event.target.innerHTML = iconComImg2;
            });

            // Bouton pour modifier - redirige sur modifyBrame
            // Append uniquement si id user = id autheur du post
            const modifyBrameButton = document.createElement('a');
            modifyBrameButton.href = '/modifyBrame/' + result.id;
            modifyBrameButton.className = "edit bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconEdPath = require('../images/edit.png');
            const iconEdImg = `<img src="${iconEdPath.default}" alt="edit">`;
            const iconEdPath2 = require('../images/edit2.png');
            const iconEdImg2 = `<img src="${iconEdPath2.default}" alt="edit">`;

            modifyBrameButton.innerHTML = iconEdImg2;

            modifyBrameButton.addEventListener("mouseover", function (event) {
                event.target.innerHTML = iconEdImg;
            });
            modifyBrameButton.addEventListener("mouseleave", function (event) {
                event.target.innerHTML = iconEdImg2;
            });

            // Bouton pour supprimer - fetch l'api pour supprimer le post
            // Append uniquement si id user = id autheur du post
            const deleteBrameButton = document.createElement('a');
            deleteBrameButton.href = '#';
            deleteBrameButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconSupPath = require('../images/trash.png');
            const iconSupImg = `<img src="${iconSupPath.default}" alt="delete">`;
            const iconSupPath2 = require('../images/trash2.png');
            const iconSupImg2 = `<img src="${iconSupPath2.default}" alt="delete">`;

            deleteBrameButton.innerHTML = iconSupImg2;

            deleteBrameButton.addEventListener("mouseover", function (event) {
                event.target.innerHTML = iconSupImg;
            });
            deleteBrameButton.addEventListener("mouseleave", function (event) {
                event.target.innerHTML = iconSupImg2;
            });

            // appends
            buttonDiv.append(likesCounter, likeButton, commentButton);
            if (userId == result.userId) {
                buttonDiv.append(modifyBrameButton, deleteBrameButton);
            }

            // Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
            // Icons made by <a href="https://www.flaticon.com/authors/gregor-cresnar" title="Gregor Cresnar">Gregor Cresnar</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
            // Icons made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>   

            // ACTION LORSQUE CLICK SUR BOUTON COMMENTAIRE
            const commentBrameForm = document.getElementById('commentBrameForm'); // form

            commentButton.addEventListener('click', function () {

                if (commentBrameForm.classList.contains('clicked')) {
                    commentBrameForm.classList.remove('clicked');
                } else {
                    commentBrameForm.classList.add('clicked');
                }
            })

            // Lorsque click sur le bouton supprimer
            deleteBrameButton.addEventListener('click', function () {
                // FETCH POUR SUPPRIMER LE POST EN BASE DE DONNEES
                fetch(`/api/posts/${result.id}`, {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'delete'
                    })
                    .then(function (res) {
                        console.log('test');
                        window.location.href = '/';
                    })
                    .catch(function (res) {
                        console.log(res)
                    })
            })
        }
    })