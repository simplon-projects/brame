const likeSystem = (likeButton, currentConnectedUserId, usersLikesInPostEntity, postRoute, likesCounter) => {
	likeButton.addEventListener('click', e => {
		e.preventDefault();

		if (currentConnectedUserId == "Not connected") {
			window.alert('Vous devez être connecté pour pouvoir donner votre avis.')
		}
		else {
			let alreadyLiked = false; // si id n'est pas dans le tableau (-> l'ajouter)

			for (let i = 0; i < usersLikesInPostEntity.length; ++i) {
				let id = usersLikesInPostEntity[i];

				if (id == ("/api/users/" + currentConnectedUserId)) {
					alreadyLiked = true;
				}
			}

			if (alreadyLiked == false) {
				usersLikesInPostEntity.push("/api/users/" + currentConnectedUserId);


				fetch(`api/posts/${postRoute.id}`, {
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						method: 'put',

						body: JSON.stringify({
							usersLikes: usersLikesInPostEntity
						})
					}) /* fetch */
					.then(() => {
						likesCounter.innerText = usersLikesInPostEntity.length;
					}) /* then */
					.catch(function (res) {
						console.log(res)
					}) /* catch */
			} /* if (alreadyLiked == false) */
			else if (alreadyLiked) {
				let indexOfId = usersLikesInPostEntity.indexOf("/api/users/" + currentConnectedUserId);
				usersLikesInPostEntity.splice(indexOfId, 1);
				// OU !
				// Pour retourner un tableau des éléments enlevés(--> peut être utilisé ensuite pour marquer "je n'aime plus" à la place de "j'aime" - mais nécessite une entité like afin de stocker les "already likes-unliked") :
				// let userUnlike = usersLikesInPostEntity.splice(indexOfId, 1); // 

				fetch(`api/posts/${postRoute.id}`, {
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						method: 'put',
						body: JSON.stringify({
							usersLikesInPostEntity: usersLikesInPostEntity
						})
					})
					.then(() => {
						likesCounter.innerText = usersLikesInPostEntity.length;
					})
					.catch(function (res) {
						console.log(res)
					}) /* fetch/then/catch */
			} /* else if (alreadyLiked) */
		} /* else if user is not/connected */
	}) /* eventlistener */
} /* likeSystem() */

export { likeSystem };