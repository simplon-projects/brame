// Variable de la div html '#hashtagValue'
const listHashtags = document.getElementById('hashtagValue');

// Valeur du hashtag selectionne
const hashtagQuery = listHashtags.dataset.id;

// FETCH POUR AFFICHER LE HASHTAG SELECTIONNE
fetch(`/api/posts?hashtag=${hashtagQuery}`)
    .then((response) => response.json(),
    )
    .then((result) => {

        // A supprimer avant rendu final
        console.log('fetch du hashtag :', result);

        // Récupère les informations
        const data = result['hydra:member'];

        for(i = 0; i < data.length; i++) {

            const result = data[i];

            // CREATION DE TOUS LES ÉLÉMENTS HTML
            const brame = document.createElement('div');
            brame.className = 'py-3 my-2 border-top border-secondary d-flex flex-column';
            listHashtags.appendChild(brame);

            const brameAuteur = document.createElement('div');
            brameAuteur.className = 'd-flex flex-row  flex-wrap pb-2';
            const auteurName = document.createElement('p');
            auteurName.className = 'text-primary font-weight-bold py-0 my-0 pr-2';
            brameAuteur.appendChild(auteurName);
            const auteurPseudo = document.createElement('a');
            auteurPseudo.className = 'pseudoPostIndex py-0 my-0 px-0';
            brameAuteur.appendChild(auteurPseudo);

            const createdAt = document.createElement('div');
            const datetime = document.createElement('p');
            datetime.className = 'datePostIndex text-primary pb-2 my-0';
            createdAt.appendChild(datetime);

            const numberCom = document.createElement('a');
            const numComContent = document.createElement('p');
            numComContent.className = 'numComment';
            numberCom.appendChild(numComContent);


            const brameMessageLink = document.createElement('a');
            const brameMessageDiv = document.createElement('div');
            brameMessageDiv.className = 'messagePostIndex bg-secondary p-2 my-1 shadow-sm rounded text-primary';
            brameMessageLink.appendChild(brameMessageDiv);
            const brameMessage = document.createElement('p');
            brameMessageDiv.appendChild(brameMessage);

            brame.append(brameAuteur, createdAt, numberCom, brameMessageLink);

            // FETCH POUR RÉCUPÉRER LE NOMBRE DE COMMENTAIRES D'UN POST
            numberCom.href = '/displayBrame/'+result.id;
            const postId = result.id;
            fetch(`/api/comments?postId=${postId}`)
                .then((response) => response.json(), // Si la requete est valide
                )
                .then((result) => {
                    // A supprimer avant rendu final
                    console.log('fetch des commentaires :', result);
                    // Récupère les informations
                    const numComment = result['hydra:totalItems'];
                    if (numComment < 2) {
                        numComContent.innerText = `${numComment} commentaire`;
                    } else {
                        numComContent.innerText = `${numComment} commentaires`;
                    }
                });
            
            // date et heure de création du post
            const date = new Date(result.createdAt);
            const dateOfCreation = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();
            const timeOfCreation = date.getHours()+"h"+date.getMinutes();
            datetime.innerText = dateOfCreation + " - " + timeOfCreation;

            // contenu du post (message) dans un lien
            brameMessageLink.href = '/displayBrame/'+result.id;
            brameMessage.innerHTML = result.message;


            // div pour les boutons
            const buttonDiv = document.createElement('div');
            buttonDiv.className = 'd-flex flex-row flex-wrap justify-content-end my-3';
            brame.appendChild(buttonDiv);
        
            // bouton pour liker - work in progress
            const likeButton = document.createElement('a');
            likeButton.href = '#';
            likeButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconLikePath =  require('../images/like.png');
            const iconLikeImg = `<img src="${iconLikePath.default}" alt="like">`;
            const iconLikePath2 =  require('../images/like2.png');
            const iconLikeImg2 = `<img src="${iconLikePath2.default}" alt="like">`;

            likeButton.innerHTML = iconLikeImg2;
            
            likeButton.addEventListener("mouseover", function( event ) {
                event.target.innerHTML = iconLikeImg;
            });
            likeButton.addEventListener("mouseleave", function( event ) {   
                event.target.innerHTML = iconLikeImg2;
            });
        
            // Bouton pour commenter - redirige sur displayBrame
            const commentButton = document.createElement('a');
            commentButton.href = '/displayBrame/'+result.id;
            commentButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconComPath =  require('../images/comment.png');
            const iconComImg = `<img src="${iconComPath.default}" alt="comment">`;
            const iconComPath2 =  require('../images/comment2.png');
            const iconComImg2 = `<img src="${iconComPath2.default}" alt="comment">`;

            commentButton.innerHTML = iconComImg2;
            
            commentButton.addEventListener("mouseover", function( event ) {
                event.target.innerHTML = iconComImg;
            });
            commentButton.addEventListener("mouseleave", function( event ) {   
                event.target.innerHTML = iconComImg2;
            });
        
            // Bouton pour modifier - redirige sur modifyBrame
            // Append uniquement si id user = id autheur du post
            const modifyBrameButton = document.createElement('a');
            modifyBrameButton.href = '/modifyBrame/'+result.id;
            modifyBrameButton.className = "edit bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconEdPath =  require('../images/edit.png');
            const iconEdImg = `<img src="${iconEdPath.default}" alt="edit">`;
            const iconEdPath2 =  require('../images/edit2.png');
            const iconEdImg2 = `<img src="${iconEdPath2.default}" alt="edit">`;

            modifyBrameButton.innerHTML = iconEdImg2;
            
            modifyBrameButton.addEventListener("mouseover", function( event ) {
                event.target.innerHTML = iconEdImg;
            });
            modifyBrameButton.addEventListener("mouseleave", function( event ) {   
                event.target.innerHTML = iconEdImg2;
            });
        
            // Bouton pour supprimer - fetch l'api pour supprimer le post
            // Append uniquement si id user = id autheur du post
            const deleteBrameButton = document.createElement('a');
            deleteBrameButton.href = '#';
            deleteBrameButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

            const iconSupPath =  require('../images/trash.png');
            const iconSupImg = `<img src="${iconSupPath.default}" alt="delete">`;
            const iconSupPath2 =  require('../images/trash2.png');
            const iconSupImg2 = `<img src="${iconSupPath2.default}" alt="delete">`;

            deleteBrameButton.innerHTML = iconSupImg2;
            
            deleteBrameButton.addEventListener("mouseover", function( event ) {
                event.target.innerHTML = iconSupImg;
            });
            deleteBrameButton.addEventListener("mouseleave", function( event ) {   
                event.target.innerHTML = iconSupImg2;
            });
        
            buttonDiv.append(commentButton, likeButton);
            
            // Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
            // Icons made by <a href="https://www.flaticon.com/authors/gregor-cresnar" title="Gregor Cresnar">Gregor Cresnar</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
            // Icons made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>        

            // FETCH POUR AFFICHER LES INFOS DU PROFIL
            fetch(`${result.authorId}`)
                .then((response) => response.json(),
                )
                .then((result) => {

                    // nom prenom et pseudo de l'auteur
                    auteurName.innerHTML = `${result.firstName} ${result.lastName}`;
                    auteurPseudo.href = '/displayProfil/'+result.id;
                    auteurPseudo.innerHTML = '@'+result.pseudo;
                    // Si ID user connecté = ID user author du post display les bouttons
                    if(userId && userId === result.id)
                    {
                        buttonDiv.appendChild(modifyBrameButton);
                        buttonDiv.appendChild(deleteBrameButton);
                    }
                })

            // Lorsque click sur le bouton supprimer
            deleteBrameButton.addEventListener('click', function() {

                // FETCH POUR SUPPRIMER LE POST EN BASE DE DONNEES
                fetch(`/api/posts/${result.id}`, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'delete'
                })
                    .then(function(res){
                        console.log('test');
                        window.location.href = '/';
                    })
                    .catch(function(res){console.log(res)})
            })
        }
    });