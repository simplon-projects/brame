// Variable du formulaire html '#newBrameForm'
const newBrameForm = document.getElementById('newBrameForm');

// LORSQUE LE FORMULAIRE EST SUBMIT
newBrameForm.addEventListener('submit', function() {

    const inputMessage = document.getElementById('messageBrame').value;
    const hashtag = document.getElementById('brameHashtag').value;

    // FETCH POUR ECRIRE LE POST EN BASE DE DONNEES
    fetch('/api/posts', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'POST',

        // Ici les informations que l'on rentre en bdd = nomDeLaTable: variableAvecLinfo
        body:JSON.stringify({message: inputMessage, hashtag: hashtag})
    })
        .then(function(res){
            console.log('test');
            window.location.href = '/';
        })
        .catch(function(res){console.log(res)})
})