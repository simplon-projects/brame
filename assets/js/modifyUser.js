// Variable de la div html '#modifyUserForm'
const modifyUserForm = document.getElementById('modifyUserForm');

// Valeur ID du post selectionne
const userId = modifyUserForm.dataset.id;

// FETCH POUR AFFICHER LES VALEURS DU POST A MODIFIER
fetch(`/api/users/${userId}`)
    .then((response) => response.json(),
    )
    .then((result) => {

        const modifyUserFirstName = document.getElementById('modifyUserFirstName');
        modifyUserFirstName.value = result.firstName;

        const modifyUserLastName = document.getElementById('modifyUserLastName');
        modifyUserLastName.value = result.lastName;

        const modifyUserPseudo = document.getElementById('modifyUserPseudo');
        modifyUserPseudo.value = result.pseudo;

        const modifyUserEmail = document.getElementById('modifyUserEmail');
        modifyUserEmail.value = result.email;

        console.log(result.id);

        // LORSQUE LE FORMULAIRE EST SUBMIT
        modifyUserForm.addEventListener('submit', function() {

            const inputModifyUserFirstName = document.getElementById('modifyUserFirstName').value;
            const inputModifyUserLastName = document.getElementById('modifyUserLastName').value;
            const inputModifyUserPseudo = document.getElementById('modifyUserPseudo').value;
            const inputModifyUserEmail = document.getElementById('modifyUserEmail').value;

            // FETCH POUR RE-ECRIRE LE POST EN BASE DE DONNEES
            fetch(`/api/users/${userId}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'PUT',

// Ici les informations que l'on rentre en bdd = nomDeLaTable: variableAvecLinfo
                body:JSON.stringify({firstName: inputModifyUserFirstName, lastName: inputModifyUserLastName,
                    pseudo: inputModifyUserPseudo, email: inputModifyUserEmail})
            })
                .then(function(res){
                    console.log('test');
                    window.location.href = '/displayUser/'+result.id;
                })
                .catch(function(res){console.log(res)})
        });
    })


