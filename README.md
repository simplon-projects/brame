# TP10 - Brame

**A rendre pour le 06/11/2020**

### Initialiser le projet

```shell
composer install
```

```shell
npm install
```

```shell
npm run watch
```

## Projet Agile

Ce projet a été réalisé en 10 jours par @audrey.eyrolles.simplon, @chloe.dubois, @olga.padkovenka et @kevin.wolff dans le cadre de la méthologie Agile, il sera présenté lors du passage de la certification Agilité. 

## Consignes

Vous devrez réaliser le backend de ce projet avec le framework Symfony. Tout le backend devra s'organiser en API de type REST. Aucune obligation pour les technologies front.

Vous devrez créer la maquette de votre et mettre l'accent sur le responsive design, l'application est destinée à un utilisation mobile.

## Contexte du projet

- Le site est composé de membres, qui peuvent s'inscrire et se connecter. Un email valide et unique est nécessaire. 

- Les membres peuvent poster des messages (<140 caractères). Un message peut contenir du texte mais aussi une image, une vidéo ou un fichier audio.

- Chaque message peut être liké par les membres. Un compteur affichera le nombre de like.

- Chaque message peut avoir des commentaires, un compteur affichera le nombre de commentaires. Ces commentaires sont créés par des membres. Les commentaires ne peuvent contenir que du texte.

- Chaque page disposera d'une URL unique et facilement identifiable, cette URL doit être partageable.

- Chaque message peut contenir un ou plusieurs hashtags. Un hashtag peut être dans ou plusieurs messages. Les hashtags sont présentés par un #.

- Une page doit permettre de voir le profil d'un membre. Dans cette page on voit les messages de cet utilisateur. Les profils des membres sont publics mais les non-membres ne peuvent pas interagir avec le profil.

- On peut cliquer les hashtags, qui nous amène sur une page contenant tous les messages avec ce hashtag.